from adafruit_circuitplayground.express import cpx
import time


def converter(temp):
    converted = 0 + ((temp - 0) * (10 - 0) / (40 - 0))
    return int(round(converted, 0))


x = 0
while True:
    col = (0, 10, 0)
    x = cpx.temperature
    cpx.pixels.brightness = .5
    if cpx.touch_A3:
        print(round(x,1))
        if x < 17:
            col = (0, 0, 10)
        elif x > 25:
            col = (10, 0, 0)
        led = converter(x)
        #print(led)
        #print(col)
        cpx.pixels.brightness = .5
        cpx.pixels[led] = col
        time.sleep(5)
        print("touch A3 to read temperature")
        cpx.pixels.fill(0)